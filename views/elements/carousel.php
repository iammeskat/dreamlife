<section class="hero-section">
    <div class="hero-slider owl-carousel" >
        <?php
        include('connection.php');
        $banner=mysqli_query($connection,'select * from banners where is_active=1');
        foreach($banner as $row) {
            ?>
            <div class="hs-item set-bg" data-setbg="<?php echo $row['picture']; ?>" >
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-7 text-white">
                            <span>Popular Shop</span>
                            <h2><?php echo $row['title']; ?></h2>
                            <p><?php echo $row['promotional_message']; ?></p>
                            <a href="<?php echo $row['link']; ?>" class="site-btn sb-line">DISCOVER</a>
                            <a href="<?php echo $row['link']; ?>" class="site-btn sb-white">ADD TO CART</a>
                        </div>
                    </div>
                    <div class="offer-card text-white">
                        <span>from</span>
                        <h2>$29</h2>
                        <p>SHOP NOW</p>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>
    </div>
    <div class="container">
        <div class="slide-num-holder" id="snh-1"></div>
    </div>
</section>
