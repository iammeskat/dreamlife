<!-- Header section -->
	<header class="header-section">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 text-center text-lg-left">
						<!-- logo -->
						<a href="./index.html" class="site-logo">
							<img src="img/logo.png" alt="">
						</a>
					</div>
					<div class="col-xl-6 col-lg-5">
						<form class="header-search-form">
							<input type="text" placeholder="Search on Search on Product....">
							<button><i class="flaticon-search"></i></button>
						</form>
					</div>
					<div class="col-xl-4 col-lg-5">
						<div class="user-panel">
							<div class="up-item">
                                <?php
                                if(!isset($_SESSION['UserID'])) {
                                    echo "<i class='flaticon-profile'></i>";
                                    echo "<a href='login.php'>Sign in</a> or <a href='SingUp.php'>Create Account</a>";
                                }
                                else{
                                    include('connect.php');
                                    $UserID = $_SESSION['UserID'];
                                    $data = $pdo->query("SELECT * FROM users WHERE id = '$UserID'")->fetch();
                                    echo "<img src=".$data['picture']." style='width:40px; height: 40px;border-radius: 100%'>";
                                    echo "<a href='profile.php'> ".$data['full_name']."</a> || <a href='logout.php'>Logout</a>";
                                }
								?>
							</div>
							<div class="up-item">
								<div class="shopping-card">
                                    <?php
                                    include_once 'includes/connection.php';
                                    $sid = $_SESSION['guest'];
                                    $data = $pdo->query("SELECT id FROM  carts where sid = '$sid'")->rowCount();
                                    ?>
									<i class="flaticon-bag"></i>
									<span><?php echo $data; ?></span>
								</div>
								<a href="cart.php">Cart</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="main-navbar">
			<div class="container">
				<!-- menu -->
				<ul class="main-menu">
					<li><a href="index.php">Home</a></li>
                    <li><a href="allproduct.php">All Product</a></li>
                    <li><a href="#">Categories</a>
                        <ul class="sub-menu">
                            <li><a href="women.php">Women</a></li>
                            <li><a href="men.php">Men</a></li>
                            <li><a href="http://localhost:63342/DreamLife/shoes.html">Shoes</a></li>
                            <li><a href="men.php">Electronics</a></li>
                            <li><a href="men.php">Furniture</a></li>
                            <li><a href="men.php">Jewellery</a></li>
                        </ul>
                    </li>
					<li><a href="#">Pages</a>
						<ul class="sub-menu">
							<li><a href="./product.php">Product Page</a></li>
							<li><a href="./category.php">Category Page</a></li>
							<li><a href="./cart.php">Cart Page</a></li>
							<li><a href="./checkout.php">Checkout Page</a></li>
							<li><a href="./contact.php">Contact Page</a></li>
						</ul>
					</li>
					<li><a href="myorder.php">My Order</a></li>
					<li><a href="contact.php">Contact Us</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Header section end -->