<?php
include_once 'connection.php';
$admin_id = $_SESSION['AdminID'];
$data = $pdo->query("SELECT * FROM admins WHERE id='$admin_id'")->fetch();
?>
<nav role="navigation" class="navbar navbar-custom">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">DreamLife-Admin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="http://localhost/dreamLife/adminpanel/pages/order"><i class="glyphicon glyphicon-shopping-cart"></i> Order</a></li>
                <li class="active"><a href="http://localhost/dreamLife/adminpanel/pages/contact"><i class="glyphicon glyphicon-envelope"></i> Message</a></li>
                <li class="active"><a href="http://localhost/dreamLife/adminpanel/pages/admin"><i class="glyphicon glyphicon-user"></i> Admin</a></li>
                <!-- <li class="disabled"><a href="#">Link</a></li> -->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img id='propic' src="http://localhost/dreamLife/adminpanel/<?php echo $data['picture']; ?>"> <?php echo $data['name']; ?> <b class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li class="dropdown-header"><a href="http://localhost/dreamLife/adminpanel/pages/profile?id=<?php echo $data['id']; ?>">Profile</a></li>
                        <li class="divider"></li>
                        <li class="disabled"><a href="http://localhost/dreamLife/adminpanel/logout.php">Signout</a></li>
                    </ul>
                </li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>