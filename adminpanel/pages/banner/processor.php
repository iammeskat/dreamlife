<?php
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<?php
//$connection=mysqli_connect('localhost','root','','dreamlife');
include('../../elements/connection.php');
date_default_timezone_set('Asia/Dhaka');
$date = date('Y/m/d h:i:s', time());

$banner = new banner($pdo);

// Add Banner
if(isset($_POST['AddBanner']))
    $banner->insert($date);

// Update Banner
else if(isset($_POST['UpdateBanner']))
    $banner->update($date);

// Delete Banner
else if(isset($_REQUEST['delete']))
    $banner::delete($pdo,$_REQUEST['delete']);

// IS Active
else if(isset($_REQUEST['activeno']))
    $banner::status($pdo,"is_active",0,$_REQUEST['activeno']);
else if(isset($_REQUEST['activeyes']))
    $banner::status($pdo,'is_active',1,$_REQUEST['activeyes']);

// IS Draft
else if(isset($_REQUEST['draftno']))
    $banner::status($pdo,"is_draft",0,$_REQUEST['draftno']);
else if(isset($_REQUEST['draftyes']))
    $banner::status($pdo,"is_draft",1,$_REQUEST['draftyes']);

// Soft Delete
else if(isset($_REQUEST['sdeleteno']))
    $banner::status($pdo,"soft_delete",0,$_REQUEST['sdeleteno']);
else if(isset($_REQUEST['sdeleteyes']))
    $banner::status($pdo,"soft_delete",1,$_REQUEST['sdeleteyes']);

// Max Display
else if(isset($_REQUEST['mdisplayno']))
    $banner::status($pdo,"max_display",0,$_REQUEST['mdisplayno']);
else if(isset($_REQUEST['mdisplayyes']))
    $banner::status($pdo,"max_display",1,$_REQUEST['mdisplayyes']);

class banner{
    public $pdo=null;
    function __construct($pdo1){
        $this->pdo=$pdo1;
    }
    function insert($date){
        $title = $_POST['title'];
        $link = $_POST['link'];
        $pmessage = $_POST['promotional_message'];
        $html_banner = $_POST['html_banner'];
        $is_active = $_POST['is_active'];
        $is_draft = $_POST['is_draft'];
        $soft_delete = $_POST['sdelete'];
        $max_display = $_POST['max_display'];

        $picture = "img/banner/".$_FILES['picture']['name'];
        $filename= $_FILES['picture']['tmp_name'];

        $sql = "INSERT INTO banners (title, picture, link, promotional_message, html_banner, is_active, is_draft, soft_delete, max_display, created_at,modified_at)
            VALUES ('$title', '$picture','$link','$pmessage','$html_banner','$is_active','$is_draft','$soft_delete','$max_display','$date','$date');";

        if((move_uploaded_file($filename, "../../../public/".$picture))&&($this->pdo->query($sql))){
            header("location: ../banner");
        }
        else echo "failed";

    }

    function update($date){
        $id = $_POST['id'];
        $title = $_POST['title'];
        $link = $_POST['link'];
        $pmessage = $_POST['promotional_message'];
        $html_banner = $_POST['html_banner'];
        $is_active = $_POST['is_active'];
        $is_draft = $_POST['is_draft'];
        //$soft_delete = $_POST['soft_delete'];
        $max_display = $_POST['max_display'];

        $picture = "img/banner/".$_FILES['picture']['name'];
        $filename= $_FILES['picture']['tmp_name'];

        $sql = "update banners set title='$title', picture='$picture', link='$link', promotional_message='$pmessage', html_banner='$html_banner',
    is_active='$is_active', is_draft='$is_draft', soft_delete='$soft_delete', max_display='$max_display', modified_at='$date' where id='$id'";

        if((move_uploaded_file($filename, "../../../public/".$picture))&&($this->pdo->query($sql))){
            header("location: ../banner");
        }

        else{
            $sql = "update banners set title='$title', link='$link', promotional_message='$pmessage', html_banner='$html_banner',
    is_active='$is_active', is_draft='$is_draft',  max_display='$max_display', modified_at='$date' where id='$id'";

            if($this->pdo->query($sql))
                header('location: ../banner');
            else echo "failed";
        }
    }

    function delete($pdo,$id){
        $sql = "delete from  banners where id = '$id'";
        if ($pdo->query($sql)) {
            header("location: ../banner");
        }
        else echo "failed";
    }

    function status($pdo,$colum,$value,$id){
        $sql = "update  banners set $colum='$value' where id = '$id'";
        if ($pdo->query($sql)) {
            header("location: ../banner");
        }
        else echo "failed";
    }
}
?>
