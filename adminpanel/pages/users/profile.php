<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Users Profile | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        img{

        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Users Profile</h3>
                </div>
                <div class="panel-body">
                    <?php
                    if(isset($_REQUEST['id'])) {
                        $id = $_REQUEST['id'];
                        $data = $pdo->query("SELECT * FROM  users where id = '$id'")->fetch();

                        ?>
                        <div class="col-md-8 col-md-offset-2">
                            <h5><?php echo $data['full_name']?></h5>
                            <div class="row" id="gradient">
                                <div class="col-md-4">
                                    <img src="../../../public/<?php echo $data['picture']?>" class="img-responsive">
                                </div>
                                <div class="col-md-8" id="overview">
                                    <table class="table ">
                                        <thead>
                                        <tr><th>Users ID :</th><th><?php echo $data['id']?></th></tr>
                                        <tr><th>Username :</th><th><?php echo $data['username']?></th></tr>
                                        <tr><th>E-mail :</th><th><?php echo $data['email']?></th></tr>
                                        <tr><th>Phone :</th><th><?php echo $data['phone']?></th></tr>
                                        <tr><th>Gender :</th><th><?php echo $data['gender']?></th></tr>
                                        <tr><th>Date of Birth :</th><th><?php echo $data['dob']?></th></tr>
                                        <tr><th>Religion :</th><th><?php echo $data['religion']?></th></tr>
                                        <tr><th>City :</th><th><?php echo $data['city']?></th></tr>
                                        <tr><th>Address :</th><th><?php echo $data['address']?></th></tr>
                                        <tr><th>Status :</th><th><?php echo $data['is_active']?></th></tr>
                                        <tr><th>Password :</th><th><?php echo $data['password']?></th></tr>
                                        <tr><th>Created at :</th><th><?php echo $data['created_at']?></th></tr>
                                        <tr><th>Modified at :</th><th><?php echo $data['modified_at']?></th></tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->
</body>
</html>
