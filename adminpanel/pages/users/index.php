<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Users List | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        #ppic{
            width: 30px;
            height: 30px;
            border-radius: 100%;
        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Users List</h3>
                </div>
                <div class="panel-body">
                    <div class="">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Status</th>
                                <th>picture</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>E-mail</th>
                                <th>Phone</th>
                                <th>Gender</th>
                                <th>Date of Birth</th>
                                <th>Religion</th>
                                <th>City</th>
                                <th>Created at</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $data = $pdo->query("SELECT * FROM  users")->fetchAll();
                            foreach ($data as $row) {
                                ?>

                                <tr>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Option<span class="caret"></span></button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="profile.php?id=<?php echo $row['id']?>">Profile</a></li>
                                                <li><?php if($row['is_active'])
                                                    echo "<a href='processor.php?de=".$row['id']."'>Deactive</a>";
                                                    else
                                                    echo "<a href='processor.php?ac=".$row['id']."'>Active</a>";
                                                    ?>
                                                </li>
                                                <li><a href="processor.php?del=<?php echo $row['id']?>">Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <th ><?php
                                        if($row['is_active'])
                                             echo "Activated";
                                        else echo "Dactivated";
                                        ?>
                                    </th>
                                    <th>
                                        <a href="profile.php?id=<?php echo $row['id']?>">
                                        <img id="ppic" src="../../../public/<?php echo $row['picture']?>"></a>
                                    </th>
                                    <th ><?php echo $row['full_name']?></th>
                                    <th> <?php echo $row['username']?> </th>
                                    <th> <?php echo $row['email']?> </th>
                                    <th> <?php echo $row['phone']?> </th>
                                    <th> <?php echo $row['gender']?> </th>
                                    <th> <?php echo $row['dob']?> </th>
                                    <th> <?php echo $row['religion']?> </th>
                                    <th> <?php echo $row['city']?> </th>
                                    <th> <?php echo $row['created_at']?> </th>
                                </tr>

                            <?php    } ?>

                            </tbody>
                        </table>
                    </div>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->
</body>
</html>
