<?php
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<?php
include('../../elements/connection.php');
date_default_timezone_set('Asia/Dhaka');
$date = date('Y/m/d h:i:s', time());

$labels = new labels();

// Add Brand
if(isset($_POST['addlabels']))
    $labels::insert($pdo);


// Update
else if(isset($_POST['update']))
    $labels::update($pdo,$date);

// Delete
else if(isset($_REQUEST['delete']))
    $labels::delete($pdo,$_REQUEST['delete']);

class labels
{

    function insert($pdo)
    {
        $title = $_POST['name'];
        $picture = "img/labels/".$_FILES['picture']['name'];
        $filename= $_FILES['picture']['tmp_name'];

        $sql = "INSERT INTO labels (title, picture) VALUES ('$title', '$picture');";

        if((move_uploaded_file($filename, "../../../public/".$picture))&&($pdo->query($sql))){
            header("location: ../labels");
        }
        else echo "failed";
    }

    function delete($pdo, $id)
    {
        if ($pdo->query("DELETE FROM labels WHERE id='$id'"))
            header("location: ../labels");
        else
            echo "Failed";
    }

    function update($pdo)
    {
        $id = $_POST['id'];
        $title = $_POST['name'];
        $picture = "img/labels/".$_FILES['picture']['name'];
        $filename= $_FILES['picture']['tmp_name'];

        if(move_uploaded_file($filename, "../../../public/".$picture)) {
            $sql = "update labels set title='$title', picture='$picture' where id = '$id'";
            if ($pdo->query($sql))
                header("location: ../labels");
            else echo "Failed";
        }
        else{
            $sql = "update labels set title='$title' where id = '$id'";
            if ($pdo->query($sql))
                header("location: ../labels");
            else echo "Failed";
        }
    }
}
?>