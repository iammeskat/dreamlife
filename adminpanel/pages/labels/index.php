<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Labels List | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        img{
            width: 90px;
            height: 50px;
        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Labels List</h3>
                </div>
                <div class="panel-body">
                    <div class="">
                        <?php if(!isset($_REQUEST['edit'])){ ?>
                        <form method="post" action ="processor.php" enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="col-md-3 mb-3">
<!--                                    <label for="validationTooltip04">Labels Title</label>-->
                                    <input type="text" class="form-control" name="name" placeholder="Labels Title" required>
                                </div>
                                <div class="col-md-3 mb-3">
<!--                                    <label for="validationTooltip05">Picture</label>-->
                                    <input type="file" name="picture" id="picture" class="form-control">
                                </div>
                            </div>
                            <button type="submit" name="addlabels" class="btn btn-primary my-1">Add Labels</button>
                        </form>
                        <?php
                        }
                        else {
                            $id = $_REQUEST['edit'];
                        $data = $pdo->query("SELECT * FROM  labels where id = '$id'")->fetch();
                            ?>
                        <form method="post" action="processor.php" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
                            <div class="form-row">
                                <div class="col-md-3 mb-3">
                                    <input type="text" class="form-control" name="name" value="<?php echo $data['title']; ?>">
                                </div>
                                <div class="col-md-3 mb-3">
                                    <input type="file" name="picture" id="picture" class="form-control">
                                </div>
                            </div>
                            <button type="submit" name="update" class="btn btn-primary my-1">Update</button>
                            <a href="../labels"><button type="button" class="btn btn-danger my-1">Cancel</button></a>
                            <?php
                            }
                        ?>
                    </div>
                    <br>
                    <div class="box">
                        <table class="table ">
                            <thead>
                            <tr>
                                <th>Picture</th>
                                <th>Labels ID</th>
                                <th>Labels Title</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $data = $pdo->query("SELECT * FROM  labels")->fetchAll();
                            foreach ($data as $row) {
                                ?>

                                <tr>
                                    <th width="80px" height="50px"><img src="../../../public/<?php echo $row['picture']?>"> </th>
                                    <th ><?php echo $row['id']?></th>
                                    <th ><?php echo $row['title']?></th>
                                    <td>

                                        <a href="index.php?edit=<?php echo $row['id']?>">
                                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        </a>


                                        <a href="processor.php?delete=<?php echo $row['id']?>" id="<?php $row['id']?>">
                                            <span style="color:red" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                    </td>
                                </tr>

                            <?php    } ?>

                            </tbody>
                        </table>
                    </div>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->
</body>
</html>
