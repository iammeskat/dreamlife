<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Admins Profile | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        #ppic{
            width: 200px;
            height: 200px;
            border-radius: 100%;

        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Admins Profile</h3>
                </div>
                <div class="panel-body">
                    <div class="panel panel-primary">
                        <?php
                        $user = $pdo->query("SELECT * FROM admins where id = ".$_REQUEST['id'])->fetch();
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title">Profile of <?php echo $user['name']; ?></h3>
                        </div>
                        <div class="panel-body">
                            <div class="pull-left" style="margin-right: 20px">
                                <a href="../users/profile.php?id=<?php echo $user['id']?>">
                                    <img id="ppic" class="img-rounded" src="../../<?php echo $user['picture']?>"></a>
                            </div>
                            <div class="pull-left">
                                <a href="../users/profile.php?id=<?php echo $user['id']?>">
                                    <h3 style="margin: 0px;color: black"><?php echo $user['name']; ?></h3></a>
                                <h6>Username: <?php echo $user['username']; ?></h6>
                                <h6>E-mail: <?php echo $user['email']; ?></h6>
                                <h6>Phone: <?php echo $user['phone']; ?></h6>
                                <h6>Password: <?php echo $user['password']; ?></h6>
                                <h6>Modified at: <?php echo $user['modified_at']; ?></h6>
                                <h6>Created at: <?php echo $user['created_at']; ?></h6>
                            </div>
                            <div class="col">
                                <a href="edit.php?id=<?php echo $user['id']; ?>"><button type="button" class="btn btn-primary btn-block">
                                    <i class='fa fa-edit'></i> Edit Profile</button></a>
                            </div>

                        </div>
                    </div>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->
</body>
</html>
