<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Edit Profile | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        img{
            width: 60px;
            height: 80px;
        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Edit Profile</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php
                        $id = $_REQUEST['id'];
                        $data = $pdo->query("SELECT * FROM  admins where id = '$id'")->fetch();
                        ?>
                        <form method="post" class="form-horizontal col-md-6 col-md-offset-3" action ="processor.php" enctype="multipart/form-data">
                            <h5><?php echo $data['name']; ?></h5>
                            <div class="form-group">
                                <label for="input1" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="name"  class="form-control" id="name" value="<?php echo $data['name']; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input1" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" name="username"  class="form-control" id="name" value="<?php echo $data['username']; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input1" class="col-sm-2 control-label">Picture</label>
                                <div class="col-sm-10">
                                    <input type="file" name="picture"  class="form-control" id="name" placeholder="Picture" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input1" class="col-sm-2 control-label">E-mail</label>
                                <div class="col-sm-10">
                                    <input type="text" name="email"  class="form-control" id="name" value="<?php echo $data['email']; ?>" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input1" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="pass"  class="form-control" id="name" value="<?php echo $data['password']; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input1" class="col-sm-2 control-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" name="phone"  class="form-control" id="name" value="<?php echo $data['phone']; ?>" />
                                </div>
                            </div>

                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <input type="submit" name="Update" class="btn btn-primary col-md-2 col-md-offset-10" value="Update" />
                        </form>
                    </div>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->
</body>
</html>
