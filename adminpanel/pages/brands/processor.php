<?php
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<?php
include('../../elements/connection.php');
date_default_timezone_set('Asia/Dhaka');
$date = date('Y/m/d h:i:s', time());

$brands = new brands();

// Add Brand
if(isset($_POST['addbrand']))
    $brands::insert($pdo,$date);

// Delete
else if(isset($_REQUEST['delete']))
    $brands::delete($pdo,$_REQUEST['delete']);

// Update
else if(isset($_POST['editbrand']))
    $brands::update($pdo,$date);

// IS Active
else if(isset($_REQUEST['activeno']))
    $brands::status($pdo,"is_active",0,$_REQUEST['activeno']);
else if(isset($_REQUEST['activeyes']))
    $brands::status($pdo,'is_active',1,$_REQUEST['activeyes']);

// IS Draft
else if(isset($_REQUEST['draftno']))
    $brands::status($pdo,"is_draft",0,$_REQUEST['draftno']);
else if(isset($_REQUEST['draftyes']))
    $brands::status($pdo,"is_draft",1,$_REQUEST['draftyes']);

// Soft Delete
else if(isset($_REQUEST['sdeleteno']))
    $brands::status($pdo,"soft_delete",0,$_REQUEST['sdeleteno']);
else if(isset($_REQUEST['sdeleteyes']))
    $brands::status($pdo,"soft_delete",1,$_REQUEST['sdeleteyes']);

class brands{

    function insert($pdo,$date){
        $title = $_POST['title'];
        $link = $_POST['link'];
        $is_draft = $_POST['is_draft'];
        $is_active = $_POST['is_active'];
        $soft_delete=1;
        $sql = "INSERT INTO brands (title, link, is_draft, is_active, soft_delete, created_at)
VALUES ('$title', '$link', '$is_draft', '$is_active', '$soft_delete', '$date')";
        if($pdo->query($sql))
            header("location: ../brands");
        else
            echo "failed";
    }

    function delete($pdo,$id){
        if($pdo->query("DELETE FROM brands WHERE id='$id'"))
            header("location: ../brands");
        else
            echo "Failed";
    }

    function update($pdo,$date){
        $id = $_POST['id'];
        $title = $_POST['title'];
        $link = $_POST['link'];
        $is_draft = $_POST['is_draft'];
        $is_active = $_POST['is_active'];
        $sql = "update brands set title='$title',link='$link', is_draft='$is_draft', is_active='$is_active', modified_at='$date' where id='$id'";
        if($pdo->query($sql))
            header("location: ../brands");
        else echo "failed";
    }

    function status($pdo,$colum,$value,$id){
        $sql = "update  brands set $colum='$value' where id = '$id'";
        if ($pdo->query($sql)) {
            header("location: ../brands");
        }
        else echo "failed";
    }
}
?>

