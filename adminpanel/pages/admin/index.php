<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Categories List | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        img{
            width: 60px;
            height: 80px;
        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Categories List</h3>
                </div>
                <div class="panel-body">
                    <a href="create.php" class="btn btn-primary btn-sm" style="margin-bottom: 15px">
                        <i class="fa fa-plus"></i>Add New
                    </a>
                    <div class="">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Created</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $data = $pdo->query("SELECT * FROM  admins")->fetchAll();
                            foreach ($data as $row) {
                                ?>

                                <tr>
                                    <th ><?php echo $row['id']?></th>
                                    <th ><?php echo $row['name']?></th>
                                    <th ><?php echo $row['email']?></th>
                                    <th> <?php echo $row['created_at']?> </th>
                                    <td>
                                        <a href="edit.php?id=<?php echo $row['id']?>">
                                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                        </a>


                                        <a href="processor.php?delete=<?php echo $row['id']?>" id="<?php $row['id']?>">
                                            <span style="color:red" class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>

                                        <br>
                                    </td>
                                </tr>

                            <?php    } ?>

                            </tbody>
                        </table>
                    </div>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->
</body>
</html>
