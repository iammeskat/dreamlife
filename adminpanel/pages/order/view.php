<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Order | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        #ppic{
            width: 40px;
            height: 50px;
        }
        #user{
            width: 100px;
            height: 100px;
            border-radius: 100%;
        }
        p{
            margin: 2px;
        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Purchase Order</h3>
                </div>
                <div class="panel-body">
                    <div class="content-row">
                        <h2 class="content-row-title right">Date-Time: <?php echo $_REQUEST['date']; ?></h2>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Buyers Information</h3>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                        $user = $pdo->query("SELECT * FROM users where id = ".$_REQUEST['bid'])->fetch();
                                        ?>

                                        <div class="pull-left" style="margin-right: 20px">
                                            <a href="../users/profile.php?id=<?php echo $user['id']?>">
                                                <img id="user" class="img-rounded" src="../../../public/<?php echo $user['picture']?>"></a>
                                        </div>
                                        <div class="pull-left">
                                            <a href="../users/profile.php?id=<?php echo $user['id']?>">
                                                <h3 style="margin: 0px;color: black"><?php echo $user['full_name']; ?></h3></a>
                                            <p>Phone: <?php echo $user['phone']; ?></p>
                                            <p>E-mail: <?php echo $user['email']; ?></p>
                                            <p>City: <?php echo $user['city']; ?></p>
                                            <p>Address: <?php echo $user['address']; ?></p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Ship To</h3>
                                    </div>
                                    <?php
                                    $ship = $pdo->query("SELECT * FROM checkout where sid = ".$_REQUEST['sid'])->fetch();
                                    ?>
                                    <div class="panel-body">
                                        <h4 style="margin: 0px">Name: <?php echo $user['full_name']; ?></h4>
                                        <p>Home & Street: <?php echo $ship['address']; ?></p>
                                        <p>City: <?php echo $ship['address2']; ?></p>
                                        <p>Country: <?php echo $ship['country']; ?></p>
                                        <p>Zip Code: <?php echo $ship['zip']; ?></p>
                                        <p>Phone: <?php echo $ship['phone']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-row">
                        <div class="panel panel-info">
                            <div class="panel-heading"><h3 class="panel-title">Products List</h3></div>
                            <div class="bs-example">
                                <table class="table ">
                                    <thead>
                                    <tr>
                                        <th id="ac">Item</th>
                                        <th>Picture</th>
                                        <th>Products Name</th>
                                        <th id="ac">Unit Price</th>
                                        <th id="ac">Quantity</th>
                                        <th id="ac">Total Price</th>
                                        <th id="ac">Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $sid = $_REQUEST['sid'];
                                    $data = $pdo->query("SELECT * FROM carts where sid = '$sid' ")->fetchAll();
                                    $i=0; $qty=0; $total=0;
                                    foreach ($data as $row) {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td width="40px" height="50px"><img id="ppic" src="../../../public/<?php echo $row['picture']?>"></td>
                                            <td><?php echo $row['product_title']; ?></td>
                                            <td><?php echo $row['unite_price']; ?></td>
                                            <td><?php echo $row['qty']; $qty+=$row['qty'];?></td>
                                            <td><?php echo $row['total_price']; $total+=$row['total_price']; ?></td>
                                            <td><a href="../product/details.php?id=<?php echo $row['product_id']; ?>" class='btn btn-info btn-sm btn-flat desc' style="background-color: skyblue">
                                                    <i class='fa fa-search'></i> View</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <td colspan="3"></td>
                                    <th>Total:</th>
                                    <th><?php echo $qty; ?></th>
                                    <th><?php echo $total; ?></th>
                                    <th>&nbsp;</th>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->

</body>
</html>
