<?php include('../../elements/connection.php');
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Edit Product | DreamLife</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="../../dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="../../dist/js/site.min.js"></script>
    <style>
        img{
            width: 60px;
            height: 80px;
        }
    </style>
</head>
<body>
<!--nav-->
<?php include('../../elements/nav.php'); ?>
<!--header-->
<div class="container-fluid">

    <!--documents-->
    <div class="row row-offcanvas row-offcanvas-left">
        <?php include('../../elements/sidepanel.php'); ?>
        <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar">
                            <span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a>Edit Product</h3>
                </div>
                <div class="panel-body">
                    <?php
                    $product_id = $_REQUEST['edit'];
                    $row = $pdo->query("SELECT * FROM products WHERE id='$product_id'")->fetch();
                   // $row = mysqli_fetch_assoc($data);
                    ?>
                    <form method="post" class="form-horizontal col-md-6 col-md-offset-3" action ="processor.php" enctype="multipart/form-data">
                        <h5><?php echo $row['title']?></h5>
                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label"> Brand ID</label>
                            <div class="col-sm-10">
                                <input type="text" name="brand_id"  class="form-control" id="name" value="<?php echo $row['id']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label"> Lebel ID</label>
                            <div class="col-sm-10">
                                <input type="text" name="lebel_id"  class="form-control" id="name" value="<?php echo $row['lebel_id']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-10">
                                <input type="text" name="title"  class="form-control" id="name" value="<?php echo $row['title']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">Picture</label>
                            <div class="col-sm-10">
                                <input type="file" name="picture"  class="form-control" id="name" value="<?php echo $row['Picture']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">Short Description</label>
                            <div class="col-sm-10">
                                <input type="text" name="short_description"  class="form-control" id="name" value="<?php echo $row['short_description']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">Description</label>
                            <div class="col-sm-10">
                                <input type="text" name="description"  class="form-control" id="name" value="<?php echo $row['description']?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">Product type</label>
                            <div class="col-sm-10">
                                <input type="text" name="product_type"  class="form-control" id="name" value="<?php echo $row['product_type']?>" />
                            </div>
                        </div>

                        <div class="form-row" style="margin-bottom: 20px;">
                            <label for="input1" class="col-sm-2 control-label">Is new</label>
                            <div class="col-sm-10">
                                <label class="radio-inline"><input type="radio" name="is_new" value="1" <?php if($row['is_new']) echo 'checked' ?>>Yes</label>
                                <label class="radio-inline"><input type="radio" name="is_new" value="0" <?php if(!$row['is_new']) echo 'checked' ?>>No</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">Cost</label>
                            <div class="col-sm-10">
                                <input type="text" name="cost"  class="form-control" id="name" value="<?php echo $row['cost']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">MRP</label>
                            <div class="col-sm-10">
                                <input type="text" name="mrp"  class="form-control" id="name" value="<?php echo $row['mrp']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input1" class="col-sm-2 control-label">Special Price</label>
                            <div class="col-sm-10">
                                <input type="text" name="special_price"  class="form-control" id="name" value="<?php echo $row['special_price']?>" />
                            </div>
                        </div>
                        <input type="hidden" name="id"  value="<?php echo $row['id']?>" />
                        <input type="submit" name="update"  class="btn btn-primary col-md-2 col-md-offset-10" value="UPDATE" />
                    </form>
                </div> <!--- End Panel Body -->
            </div>

            <div class="panel panel-default">
                <h1>Dream Life Footer</h1>

            </div>
        </div>
    </div><!-- panel body -->
</div>
</div><!-- content -->
</body>
</html>
