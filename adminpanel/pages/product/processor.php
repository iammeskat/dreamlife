<?php
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<?php
//$connection=mysqli_connect('localhost','root','','dreamlife');
include('../../elements/connection.php');
date_default_timezone_set('Asia/Dhaka');
$date = date('Y/m/d h:i:s', time());

$product = new product($pdo);

// Add Product
if(isset($_POST['AddProduct']))
    $product->insert($date);

// Update Product
else if(isset($_POST['update']))
    $product->update($date);

// Delete Product
else if(isset($_REQUEST['delete']))
    $product->delete($_REQUEST['delete']);

// IS Active
else if(isset($_REQUEST['activeno']))
    $product->status("is_active",0,$_REQUEST['activeno']);
else if(isset($_REQUEST['activeyes']))
    $product->status('is_active',1,$_REQUEST['activeyes']);

// IS Draft
else if(isset($_REQUEST['draftno']))
    $product->status("is_draft",0,$_REQUEST['draftno']);
else if(isset($_REQUEST['draftyes']))
    $product->status("is_draft",1,$_REQUEST['draftyes']);

// IS New
else if(isset($_REQUEST['newno']))
    $product->status("is_new",0,$_REQUEST['newno']);
else if(isset($_REQUEST['newyes']))
    $product->status("is_new",1,$_REQUEST['newyes']);


class product{
    public $pdo=null;
    function __construct($var)
    {
        $this->pdo=$var;
    }

    function insert($date){
        $brand_id = $_POST['brand_id'];     $lebel_id = $_POST['lebel_id'];
        $title = $_POST['title'];       $short_description = $_POST['short_description'];
        $description = $_POST['description'];       $product_type = $_POST['product_type'];
        $is_new = $_POST['is_new'];     $cost = $_POST['cost'];
        $mrp = $_POST['mrp'];       $special_price = $_POST['special_price'];

        $picture = "img/product/".$_FILES['picture']['name'];
        $filename= $_FILES['picture']['tmp_name'];

        if(move_uploaded_file($filename,"../../../public/".$picture)) {


            $sql = "INSERT INTO products (brand_id,lebel_id,title,picture,short_description,description,product_type,is_new,cost,mrp,special_price,created_at,total_sales,soft_delete,is_draft,is_active,modified_at)
		VALUES ('$brand_id','$lebel_id','$title','$picture','$short_description','$description','$product_type','$is_new','$cost','$mrp','$special_price','$date',0,1,1,1,'$date')";

            if ($this->pdo->query($sql)) {
                header("location: index.php?res=1");
            } else {
                header("location: index.php?res=0");
            }
        }
            else echo "Upload Failed";


    }

    function update($date){
        $product_id = $_POST['id'];
        $brand_id = $_POST['brand_id'];     $lebel_id = $_POST['lebel_id'];
        $title = $_POST['title'];       $short_description = $_POST['short_description'];
        $description = $_POST['description'];       $product_type = $_POST['product_type'];
        $is_new = $_POST['is_new'];     $cost = $_POST['cost'];
        $mrp = $_POST['mrp'];       $special_price = $_POST['special_price'];

        $picture = "img/product/".$_FILES['picture']['name'];
        $filename= $_FILES['picture']['tmp_name'];

        if(move_uploaded_file($filename, "../../../public/".$picture)){
            $sql = "UPDATE products SET brand_id='$brand_id',lebel_id='$lebel_id',title='$title',short_description='$short_description',description='$description',product_type='$product_type',
is_new='$is_new',cost='$cost',mrp='$mrp',special_price='$special_price',picture='$picture',modified_at='$date' where id='$product_id'";

            if($this->pdo->query($sql))
                header("location: ../product");
            else echo "failed";
        }

        else{
            $sql = "update products set brand_id='$brand_id',lebel_id='$lebel_id',title='$title',short_description='$short_description',description='$description',
product_type='$product_type',is_new='$is_new',cost='$cost',mrp='$mrp',special_price='$special_price',modified_at='$date' where id='$product_id' ";

            if($this->pdo->query($sql)) header('location: ../product');
            else echo "failed1";
        }
    }

    function delete($id){

        $sql = "delete from  products where id = '$id'";
        if ($this->pdo->query($sql)) {
            header("location: ../product");
        }
        else echo "failed";
    }

    function status($colum,$value,$id){
        $sql = "update  products set $colum='$value' where id = '$id'";
        if ($this->pdo->query($sql)) {
            header("location: ../product");
        }
        else echo "failed";
    }
}
?>
