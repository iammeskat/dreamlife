<?php
session_start();
if (!isset($_SESSION['AdminID']))
{
    header("Location: ../../?login");
    die();
}
?>
<?php
include('../../elements/connection.php');
date_default_timezone_set('Asia/Dhaka');
$date = date('Y/m/d h:i:s', time());

$categories = new categories();

//Add Categories
if(isset($_POST['AddCat']))
    $categories::insert($pdo,$date);

//delete product
else if(isset($_REQUEST['delete']))
    $categories::delete($pdo,$_REQUEST['delete']);

//Update Categories
else if(isset($_POST['UpdateCat']))
    $categories::update($pdo,$date);

//is_Draft
else if (isset($_REQUEST['draftno']))
    $categories::status($pdo,'is_draft',0,$_REQUEST['draftno']);
else if (isset($_REQUEST['draftyes']))
    $categories::status($pdo,'is_draft',1,$_REQUEST['draftyes']);

//Soft Delete
else if (isset($_REQUEST['sdeleteno']))
    $categories::status($pdo,'soft_delete',0,$_REQUEST['sdeleteno']);
else if (isset($_REQUEST['sdeleteyes']))
    $categories::status($pdo,'soft_delete',1,$_REQUEST['sdeleteyes']);

class categories{
    function insert($pdo,$date){
        $name = $_POST['name'];
        $link = $_POST['link'];
        $sdelete = $_POST['soft_delete'];
        $is_draft = $_POST['is_draft'];

        $sql = "insert into categories (c_name, link, soft_delete, is_draft, created_at) VALUES ('$name', '$link', '$sdelete', '$is_draft','$date')";
        if($pdo->query($sql))
            header('location: ../categories');
        else echo "failed";
    }
    function delete($pdo,$id){
        $id = $_REQUEST['delete'];
        if($pdo->query("DELETE FROM categories WHERE id='$id'"))
            header("location: ../categories");
        else
            echo "Failed";
    }
    function update($pdo,$date){
        $id = $_POST['id'];
        $c_name = $_POST['name'];
        $link = $_POST['link'];
        $soft_delete = $_POST['soft_delete'];
        $is_draft = $_POST['is_draft'];

        $sql = "update categories set c_name='$c_name', link='$link', soft_delete='$soft_delete', is_draft='$is_draft', modified_at='$date' where id='$id'";

        if($pdo->query($sql))
            header('location: ../categories');
        else echo "failed";
    }
    function status($pdo,$colum,$value,$id){
        $sql = "update  categories set $colum='$value' where id = '$id'";
        if ($pdo->query($sql)) {
            header("location: ../categories");
        }
        else echo "failed";
    }
}
?>