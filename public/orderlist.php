<?php include('includes/connection.php'); ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>
    <style>
        #ppic{
            width: 30px;
            height: 30px;
            border-radius: 100%;
        }
    </style>



</head>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>
<!-- Page Preloder end -->

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->

<!-- Product filter section -->
<section class="product-filter-section">
    <div class="container"><br>
        <div class="content-row">
            <div class="panel panel-info">
                <div class="panel-heading"><h3 class="panel-title">Products List</h3></div>
                <div class="bs-example">
                    <table class="table ">
                        <thead>
                        <tr>
                            <th id="ac">Item</th>
                            <th>Picture</th>
                            <th>Products Name</th>
                            <th id="ac">Unit Price</th>
                            <th id="ac">Quantity</th>
                            <th id="ac">Total Price</th>
                            <th id="ac">Details</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sid = $_REQUEST['sid'];
                        $data = $pdo->query("SELECT * FROM carts where sid = '$sid' ")->fetchAll();
                        $i=0; $qty=0; $total=0;
                        foreach ($data as $row) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td width="40px" height="50px"><img id="ppic" src="<?php echo $row['picture']?>"></td>
                                <td><?php echo $row['product_title']; ?></td>
                                <td><?php echo $row['unite_price']; ?></td>
                                <td><?php echo $row['qty']; $qty+=$row['qty'];?></td>
                                <td><?php echo $row['total_price']; $total+=$row['total_price']; ?></td>
                                <td><a href="product.php?id=<?php echo $row['product_id']; ?>" class='btn btn-info btn-sm btn-flat desc' >
                                        <i class='fa fa-search'></i> View</a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <td colspan="3"></td>
                        <th>Total:</th>
                        <th><?php echo $qty; ?></th>
                        <th><?php echo $total; ?></th>
                        <th>&nbsp;</th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Product filter section end -->

<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>


