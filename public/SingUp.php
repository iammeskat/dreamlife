<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>



</head>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->


<!-- Page info -->
<div class="page-top-info">
    <div class="container">
        <h4>Contact</h4>
        <div class="site-pagination">
            <a href="">Home</a> /
            <a href="">SingUp</a>
        </div>
    </div>
</div>
<!-- Page info end -->


<!-- SingUp section -->
<section>
    <div class="container">
        <div class="card bg-light">
            <article class="card-body mx-auto" style="max-width: 400px;">
                <h4 class="card-title mt-3 text-center">Create Account</h4>
                <p class="text-center">Get started with your free account</p>
                <p>
                    <a href="" class="btn btn-block btn-twitter"> <i class="fab fa-twitter"></i>   Login via Twitter</a>
                    <a href="" class="btn btn-block btn-facebook"> <i class="fab fa-facebook-f"></i>   Login via facebook</a>
                </p>
                <p class="divider-text">
                    <span class="bg-light">OR</span>
                </p>
                <form method="post" enctype="multipart/form-data" action="insert.php">
                    <div class="form-group input-group"><!-- name -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                        </div>
                        <input name="name" class="form-control" placeholder="Full name" type="text">
                    </div>
                    <div class="form-group input-group"><!-- username -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-user-circle"></i> </span>
                        </div>
                        <input name="username" class="form-control" placeholder="Username" type="text">
                    </div>
                    <div class="form-group input-group"><!-- picture -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-upload"></i> </span>
                        </div>
                        <input name="picture" class="form-control" placeholder="Username" type="file">
                    </div>
                    <div class="form-group input-group"><!--- email -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                        </div>
                        <input name="email" class="form-control" placeholder="Email address" type="email">
                    </div>
                    <div class="form-group input-group"> <!--- phone -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                        </div>
                        <input name="phone" class="form-control" placeholder="Phone number" type="text">
                    </div>
                    <div class="form-group input-group"> <!-- gender -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-venus-mars"></i> </span>
                        </div>
                        <select class="form-control" name="gender">
                            <option selected=""> Gender </option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="form-group input-group"> <!-- dob -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-calendar-check-o"></i> </span>
                        </div>
                        <input name="dob" class="form-control" placeholder="Date Of Birth" type="date">
                    </div>
                    <div class="form-group input-group"> <!-- Religion -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-institution"></i> </span>
                        </div>
                        <select class="form-control" name="religion">
                            <option selected=""> Religion </option>
                            <option value="Islam">Islam</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="form-group input-group"> <!--- City -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-address-card"></i> </span>
                        </div>
                        <input name="city" class="form-control" placeholder="City" type="text">
                    </div>
                    <div class="form-group input-group"> <!--- Address -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-address-card"></i> </span>
                        </div>
                        <input name="address" class="form-control" placeholder="Address" type="text">
                    </div>
                    <div class="form-group input-group"> <!--- password -->
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                        </div>
                        <input name="password" class="form-control" placeholder="Create password" type="password">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary btn-block"> Create Account  </button>
                    </div> <!-- form-group// -->
                    <p class="text-center">Have an account? <a href="login.html">Log In</a> </p>
                </form>
            </article>
        </div>

    </div>
</section>
<!-- SingUp section end -->

<!-- Banner section -->
<?php include_once '../views/elements/banner.php' ?>
<!-- Banner section end  -->


<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
