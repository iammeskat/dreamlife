<?php include('includes/connection.php'); ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>
    <style>
        #ppic{
            width: 300px;
            height: 300px;
            border-radius: 100%;
        }
    </style>



</head>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>
<!-- Page Preloder end -->

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->

<!-- Product filter section -->
<section class="product-filter-section">
    <?php if(isset($_SESSION['UserID'])) { ?>
        <div class="container"><br>
            <?php
            $id = $_SESSION['UserID'];
            $i = 1;
            $data = $pdo->query("SELECT * FROM  checkout WHERE bid = '$id' order by id desc ")->fetchAll();
            ?>
            <div class="row">
                <h3 class="panel-title">Orders List</h3>
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Item</td>
                        <td>Order ID</td>
                        <td>Date</td>
                        <td>Quantity</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                    </tbody>
                    <tbody>
                    <?php foreach ($data as $row) {
                        $sid = $row['sid'];
                        $carts = $pdo->query("SELECT * FROM  carts WHERE sid = '$id'")->fetch();
                        $qty = $pdo->query("SELECT * FROM  carts WHERE sid = '$sid'")->rowCount();
                        ?>
                        <tr>
                            <td><?php echo $i;
                                $i++; ?></td>
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['date']; ?></td>
                            <td><?php echo $qty; ?></td>
                            <td><?php
                                if ($row['status'])
                                    echo "<p style='color: green'>Accepted</p>";
                                else echo "<p style='color: red'>Pending</p>";
                                ?></td>
                            <td>
                                <a href='orderlist.php?sid=<?php echo $sid; ?>' data-toggle='modal'
                                   class='btn btn-info btn-sm btn-flat desc'><i class='fa fa-search'></i> View</a>
                                <?php
                                if (!$row['status'])
                                    echo "<a href='order.php?delete=" . $row['id'] . "' data-toggle='modal' class='btn btn-danger btn-sm delete btn-flat' ><i class='fa fa-trash'></i> Delete</a>";
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

            <div class="text-center pt-5">
                <a href="editprofile.php">
                    <button class="site-btn sb-line sb-dark">Edit Profile</button>
                </a>
            </div>
        </div>
        <?php
    }
    else echo "<h3><br/> You must login to see this page</h3>";
    ?>
</section>
<!-- Product filter section end -->

<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>


