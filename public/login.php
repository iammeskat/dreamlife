
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Login | DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>
</head>
<?php
include('connect.php');
include_once 'classes/user.php';
$user = new user($pdo);

if(isset($_POST['login'])){
    if($user->login($_POST['email'],$_POST['password'])){

        $_SESSION['UserID'] = $user->ProfileID($_POST['email'],$_POST['password']);
        header('location: index.php');
    }
    else{
        echo "Login Failed";
    }

}
?>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->


<!-- Page info -->
<div class="page-top-info">
    <div class="container">
        <h4>Contact</h4>
        <div class="site-pagination">
            <a href="">Home</a> /
            <a href="">login</a>
        </div>
    </div>
</div>
<!-- Page info end -->


<!-- SingUp section -->
<section>
    <div id="logreg-forms">
        <form class="form-signin" action="login.php" method="post">
            <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Sign in</h1>
            <div class="social-login">
                <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Sign in with Facebook</span> </button>
                <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Sign in with Google+</span> </button>
            </div>
            <p style="text-align:center"> OR  </p>
            <input type="text" id="inputEmail" class="form-control" placeholder="Username Or Email" required name="email">
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">

            <button class="btn btn-success btn-block" type="submit" name="login"><i class="fas fa-sign-in-alt"></i> Sign in</button>
            <a href="#" id="forgot_pswd">Forgot password?</a>
            <hr>
            <!-- <p>Don't have an account!</p>  -->
            <button class="btn btn-primary btn-block" type="button" id="btn-signup"><i class="fas fa-user-plus"></i> Sign up New Account</button>
        </form>

        <form action="/reset/password/" class="form-reset">
            <input type="email" id="resetEmail" class="form-control" placeholder="Email address" required="" autofocus="">
            <button class="btn btn-primary btn-block" type="submit">Reset Password</button>
            <a href="#" id="cancel_reset"><i class="fas fa-angle-left"></i> Back</a>
        </form>

        <form action="/signup/" class="form-signup">
            <div class="social-login">
                <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Sign up with Facebook</span> </button>
            </div>
            <div class="social-login">
                <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Sign up with Google+</span> </button>
            </div>

            <p style="text-align:center">OR</p>

            <input type="text" id="user-name" class="form-control" placeholder="Full name" required="" autofocus="">
            <input type="email" id="user-email" class="form-control" placeholder="Email address" required autofocus="">
            <input type="password" id="user-pass" class="form-control" placeholder="Password" required autofocus="">
            <input type="password" id="user-repeatpass" class="form-control" placeholder="Repeat Password" required autofocus="">

            <button class="btn btn-primary btn-block" type="submit"><i class="fas fa-user-plus"></i> Sign Up</button>
            <a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Back</a>
        </form>
        <br>

    </div>
</section>
<!-- SingUp section end -->

<!-- Banner section -->
<?php include_once '../views/elements/banner.php' ?>
<!-- Banner section end  -->


<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
