<?php
session_start();
include_once 'includes/connection.php';
date_default_timezone_set('Asia/Dhaka');
$date = date('Y/m/d h:i:s', time());

if(isset($_POST['order'])){
    $sid =  $_SESSION['guest'];
    $bid = $_SESSION['UserID'];
    $address = $_POST['address1'];
    $address2 = $_POST['address2'];
    $country = $_POST['country'];
    $zip = $_POST['zip'];
    $phone = $_POST['phone'];
    $total_price = $_SESSION['total'];
    $status=0;

    $sql =  "insert into `checkout` (`sid`,`bid`,`address`,`address2`,`country`,`zip`,`phone`,`total_price`,`date`,`status`)
VALUES (:sid, :bid,:address,:address2,:country,:zip,:phone,:total_price,:date,:status)";
    $statement = $pdo->prepare($sql);
    $statement->bindValue(':sid', $sid);
    $statement->bindValue(':bid', $bid);
    $statement->bindValue(':address', $address);
    $statement->bindValue(':address2', $address2);
    $statement->bindValue(':country', $country);
    $statement->bindValue(':zip', $zip);
    $statement->bindValue(':phone', $phone);
    $statement->bindValue(':total_price', $total_price);
    $statement->bindValue(':date', $date);
    $statement->bindValue(':status', $status);
    $inserted = $statement->execute();
    if($inserted){
        $_SESSION['guest']=rand(111,999).time();
        header('location: myorder.php');
    }
}
else if(isset($_REQUEST['delete'])){
    $id = $_REQUEST['delete'];
    $sql = "delete from checkout where id = '$id'";
    $pdo->query($sql);
    header('location: myorder.php');
}
?>