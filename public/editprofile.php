<?php include('includes/connection.php'); ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>
    <style>
        #ppic{
            width: 300px;
            height: 300px;
            border-radius: 100%;
        }
    </style>



</head>
<?php
include('connect.php');
include_once 'classes/user.php';
$user = new user($pdo);
?>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>
<!-- Page Preloder end -->

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->

<!-- Product filter section -->
<section class="product-filter-section">
    <div class="container">
        <?php
        $data = $user->info($_SESSION['UserID'])
        ?>
        <form action="update.php" method="post" enctype="multipart/form-data">
            <div class="section-title"><br>
                <br><h4>Update your profile</h4>
                <center><table class="table">
                        <tbody>
                        <tr><td rowspan="11"><img id="ppic" src="<?php echo $data['picture']; ?>"><br><br><input type="file" name="picture"></td></tr>
                        <tr><td><h5>Full Name: </h5></td><td><input type="text" name="name" value="<?php echo $data['full_name']; ?>"></td></tr>
                        <tr><td><h5>Username: </h5></td><td><input type="text" name="username" value="<?php echo $data['username']; ?>"></td></tr>
                        <tr><td><h5>E-mail: </h5></td><td><input type="text" name="email" value="<?php echo $data['email']; ?>"></td></tr>
                        <tr><td><h5>Phone: </h5></td><td><input type="text" name="phone" value="<?php echo $data['phone']; ?>"></td></tr>
                        <tr><td><h5>Gender: </h5></td>
                            <td><select name="gender">
                                    <option>-----------Select----------</option>
                                    <option <?php if($data['gender']=='Male') echo 'selected'; ?>>Male</option>
                                    <option <?php if($data['gender']=='Female') echo 'selected'; ?>>Female</option>
                                    <option <?php if($data['gender']=='Other') echo 'selected'; ?>>Other</option>
                                    <option></option>
                                </select>
                            </td></tr>
                        <tr><td><h5>Date of Birth: </h5></td><td><input type="date" name="dob" value="<?php echo $data['dob']; ?>"></td></tr>
                        <tr><td><h5>Religion: </h5></td><td>
                                <select name="religion">
                                    <option>-----------Select----------</option>
                                    <option <?php if($data['religion']=='Islam') echo 'selected'; ?>>Islam</option>
                                    <option <?php if($data['religion']=='Hindu') echo 'selected'; ?>>Hindu</option>
                                    <option <?php if($data['religion']=='Other') echo 'selected'; ?>>Other</option>
                                    <option></option>
                                </select>
                            </td></tr>
                        <tr><td><h5>City: </h5></td><td><input type="text" name="city" value="<?php echo $data['city']; ?>"></td></tr>
                        <tr><td><h5>Address: </h5></td><td><input type="text" name="address" value="<?php echo $data['address']; ?>"></td></tr>
                        <tr><td><h5>Password: </h5></td><td><input type="password" name="pass" value="<?php echo $data['password']; ?>"></td></tr>
                        </tbody>
                    </table></center>
            </div>
            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">

            <div class="text-center pt-5">
                <a href="editprofile.php"><button class="site-btn sb-line sb-dark" name="update">Update Profile</button></a>
            </div>
        </form>
    </div>
</section>
<!-- Product filter section end -->

<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>

