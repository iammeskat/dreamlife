<?php include('connection.php'); ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>




</head>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>
<!-- Page Preloder end -->

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->

<!-- Product filter section -->
<section class="product-filter-section">
    <div class="container">
        <div class="section-title">
            <br>
            <h2>BROWSE MEN'S PRODUCTS</h2>
        </div>
        <ul class="product-filter-menu">
            <li><a href="#">TOPS</a></li>
            <li><a href="#">JUMPSUITS</a></li>
            <li><a href="#">LINGERIE</a></li>
            <li><a href="#">JEANS</a></li>
            <li><a href="#">DRESSES</a></li>
            <li><a href="#">COATS</a></li>
            <li><a href="#">JUMPERS</a></li>
            <li><a href="#">LEGGINGS</a></li>
        </ul>
        <div class="row">
            <?php
            $data = mysqli_query($connection, "SELECT * FROM products where is_active=1 and product_type='Men'");
            foreach ($data as $row) {
                ?>
                <div class="col-lg-3 col-sm-6">
                    <div class="product-item">
                        <div class="pi-pic">
                            <a href="product.php?id=<?php echo $row['id']; ?>"><img src="<?php echo $row['picture']; ?>" alt=""></a>
                            <div class="pi-links">
                                <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                                <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                            </div>
                        </div>
                        <div class="pi-text">
                            <h6>$<?php echo $row['mrp']; ?></h6>
                            <p><?php echo $row['title']; ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
        <div class="text-center pt-5">
            <button class="site-btn sb-line sb-dark">LOAD MORE</button>
        </div>
    </div>
</section>
<!-- Product filter section end -->


<!-- Banner section -->
<?php include_once '../views/elements/banner.php' ?>
<!-- Banner section end  -->


<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>

