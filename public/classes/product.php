<?php
include_once 'connection.php';
class product
{
    public $db=null;
    function __construct($db)
    {
        $this->db=$db;
    }

    function AllProduct(){
        $data = $this->db->query("SELECT * FROM products where is_active=1 ORDER BY id DESC")->fetchAll();
        return $data;
    }

    function LatestProduct(){
        $data = $this->db->query("SELECT * FROM products where is_active=1 and is_new=1 ORDER BY id DESC")->fetchAll();
        return $data;
    }

    function TopSell(){
        $data = $this->db->query("SELECT * FROM products where is_active=1 ORDER BY total_sales DESC")->fetchAll();
        return $data;
    }

    function WomensProduct(){
        $data = $this->db->query("SELECT * FROM products where is_active=1 and product_type='Women'")->fetchAll();
        return $data;
    }
}