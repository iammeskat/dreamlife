<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>


</head>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->



<!-- letest Shoes section -->
<section class="top-letest-product-section">
    <div class="container">
        <div class="section-title">
            <h2>LATEST SHOES</h2>
        </div>
        <div class="product-slider owl-carousel">
            <div class="product-item">
                <div class="pi-pic">
                    <img src="./img/shoes/1.jpg" alt="">
                    <div class="pi-links">
                        <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                        <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                    </div>
                </div>
                <div class="pi-text">
                    <h6>$35,00</h6>
                    <p>Fashion Men's Casual Soft Running Trainers Shoes </p>
                </div>
            </div>
            <div class="product-item">
                <div class="pi-pic">
                    <div class="tag-new">New</div>
                    <img src="./img/shoes/2.jpg" alt="">
                    <div class="pi-links">
                        <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                        <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                    </div>
                </div>
                <div class="pi-text">
                    <h6>$35,00</h6>
                    <p>Fashion Men's Casual Soft Running Trainers Shoes</p>
                </div>
            </div>
            <div class="product-item">
                <div class="pi-pic">
                    <img src="./img/shoes/3.jpg" alt="">
                    <div class="pi-links">
                        <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                        <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                    </div>
                </div>
                <div class="pi-text">
                    <h6>$35,00</h6>
                    <p>Fashion Men's Casual Soft Running Trainers Shoes </p>
                </div>
            </div>
            <div class="product-item">
                <div class="pi-pic">
                    <img src="./img/shoes/4.jpg" alt="">
                    <div class="pi-links">
                        <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                        <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                    </div>
                </div>
                <div class="pi-text">
                    <h6>$35,00</h6>
                    <p>Fashion Men's Casual Soft Running Trainers Shoes</p>
                </div>
            </div>
            <div class="product-item">
                <div class="pi-pic">
                    <img src="./img/shoes/5.jpg" alt="">
                    <div class="pi-links">
                        <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                        <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                    </div>
                </div>
                <div class="pi-text">
                    <h6>$35,00</h6>
                    <p>Fashion Men's Casual Soft Running Trainers Shoes</p>
                </div>
            </div>
            <div class="product-item">
                <div class="pi-pic">
                    <img src="./img/shoes/6.jpg" alt="">
                    <div class="pi-links">
                        <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                        <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                    </div>
                </div>
                <div class="pi-text">
                    <h6>$35,00</h6>
                    <p>Fashion Men's Casual Soft Running Trainers Shoes </p>
                </div>
            </div><div class="product-item">
            <div class="pi-pic">
                <img src="./img/shoes/7.jpg" alt="">
                <div class="pi-links">
                    <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                    <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                </div>
            </div>
            <div class="pi-text">
                <h6>$35,00</h6>
                <p>Fashion Men's Casual Soft Running Trainers Shoes </p>
            </div>
        </div><div class="product-item">
            <div class="pi-pic">
                <img src="./img/shoes/8.jpg" alt="">
                <div class="pi-links">
                    <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                    <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                </div>
            </div>
            <div class="pi-text">
                <h6>$35,00</h6>
                <p>Fashion Men's Casual Soft Running Trainers Shoes </p>
            </div>
        </div><div class="product-item">
            <div class="pi-pic">
                <img src="./img/shoes/1.jpg" alt="">
                <div class="pi-links">
                    <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                    <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                </div>
            </div>
            <div class="pi-text">
                <h6>$35,00</h6>
                <p>Fashion Men's Casual Soft Running Trainers Shoes</p>
            </div>
        </div><div class="product-item">
            <div class="pi-pic">
                <img src="./img/shoes/10.jpg" alt="">
                <div class="pi-links">
                    <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                    <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                </div>
            </div>
            <div class="pi-text">
                <h6>$35,00</h6>
                <p>Fashion Men's Casual Soft Running Trainers Shoes </p>
            </div>
        </div>
        </div>
    </div>
</section>
<!-- letest Shoes section end -->



<!-- Product filter section -->
<section class="product-filter-section">
    <div class="container">
        <div class="section-title">
            <h2>POPULAR TOP SELLING SHOES</h2>
        </div>
        <ul class="product-filter-menu">
            <li><a href="#">Boat </a></li>
            <li><a href="#">Blucher </a></li>
            <li><a href="#">Brogan</a></li>
            <li><a href="#">Brogue</a></li>
            <li><a href="#">Brothel</a></li>
            <li><a href="#">Bucks</a></li>
            <li><a href="#">Chopine</a></li>
            <li><a href="#">Galesh</a></li>
        </ul>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/shoes/8.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Flamboyant Pink Top </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <div class="tag-sale">ON SALE</div>
                        <img src="./img/shoes/6.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Black and White Stripes Dress</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/shoes/7.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Flamboyant Pink Top </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/shoes/8.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Flamboyant Pink Top </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/shoes/5.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Flamboyant Pink Top </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/shoes/10.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Black and White Stripes Dress</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/shoes/11.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Flamboyant Pink Top </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/shoes/12.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>$35,00</h6>
                        <p>Flamboyant Pink Top </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center pt-5">
            <button class="site-btn sb-line sb-dark">LOAD MORE</button>
        </div>
    </div>
</section>
<!-- Product filter section end -->


<!-- Banner section -->
<?php include_once '../views/elements/banner.php' ?>
<!-- Banner section end  -->


<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>
