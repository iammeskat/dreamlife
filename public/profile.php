<?php include('includes/connection.php'); ?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>
    <style>
        #ppic{
            width: 300px;
            height: 300px;
            border-radius: 100%;
        }
    </style>



</head>
<?php
include('connect.php');
include_once 'classes/user.php';
$user = new user($pdo);
?>
<body>
<!-- Page Preloder -->
<?php include_once '../views/elements/loader.php' ?>
<!-- Page Preloder end -->

<!-- Header section -->
<?php include_once '../views/elements/nav.php' ?>
<!-- End Header section -->

<!-- Product filter section -->
<section class="product-filter-section">
    <div class="container"><br>
        <?php if(isset($_REQUEST['res'])) {
            if ($_REQUEST['res'])
                echo "<p style='color: green'><strong>Well done!</strong> Profile update successful.<a href='profile.php'> X</a></p>";
            else echo "<p style='color: red'><strong>Failed!</strong> Profile update is not successful.<a href='profile.php'> X</a></p>";
        }
        ?>
        <?php
        $data = $user->info($_SESSION['UserID'])
        ?>
        <div class="section-title">
            <br><h1><?php echo $data['full_name']; ?></h1>
                <center><table class="table">
                        <tbody>
                        <tr><td rowspan="10"><img id="ppic" src="<?php echo $data['picture']; ?>"></td></tr>
                        <tr><td><h5>Username: </h5></td><td><h5><?php echo $data['username']; ?></h5></td></tr>
                        <tr><td><h5>E-mail: </h5></td><td><h5><?php echo $data['email']; ?></h5></td></tr>
                        <tr><td><h5>Phone: </h5></td><td><h5><?php echo $data['phone']; ?></h5></td></tr>
                        <tr><td><h5>Gender: </h5></td><td><h5><?php echo $data['gender']; ?></h5></td></tr>
                        <tr><td><h5>Date of Birth: </h5></td><td><h5><?php echo $data['dob']; ?></h5></td></tr>
                        <tr><td><h5>Religion: </h5></td><td><h5><?php echo $data['religion']; ?></h5></td></tr>
                        <tr><td><h5>City: </h5></td><td><h5><?php echo $data['city']; ?></h5></td></tr>
                        <tr><td><h5>Address: </h5></td><td><h5><?php echo $data['address']; ?></h5></td></tr>
                        <tr><td><h5>Created at: </h5></td><td><h5><?php echo $data['created_at']; ?></h5></td></tr>
                        </tbody>
                    </table></center>
        </div>

        <div class="text-center pt-5">
            <a href="editprofile.php"><button class="site-btn sb-line sb-dark">Edit Profile</button></a>
        </div>
    </div>
</section>
<!-- Product filter section end -->

<!-- Footer section -->
<?php include_once '../views/elements/footer.php' ?>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>

