
<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>
    <style>
        #cpic{
            height: 50px;
            width: 30px;
        }
    </style>
</head>
<?php
include_once 'includes/connection.php';
$sid = $_SESSION['guest'];
?>
<body>
	<!-- Page Preloder -->
    <?php include_once '../views/elements/loader.php' ?>

    <!-- Header section -->
    <?php include_once '../views/elements/nav.php' ?>
    <!-- End Header section -->


	<!-- cart section end -->
	<section class="cart-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="cart-table">
						<h3>Your Cart</h3>
						<div class="cart-table-warp">
							<table >
							<thead>
								<tr>
									<th class="product-th">Product</th>
									<th class="quy-th">Quantity</th>
									<th class="total-th">Price</th>
									<th class="total-th">Action</th>
								</tr>
							</thead>
							<tbody>
                            <?php
                            $data = $pdo->query("SELECT * FROM  carts WHERE sid =  '$sid'")->fetchAll();
                            $_SESSION['total']=0;
                            foreach ($data as $row) {
                                ?>
                                <tr>
                                    <td class="product-col">
                                        <img id="cpic" src="<?php echo $row['picture'] ?>" alt="">
                                        <div class="pc-title">
                                            <h4><?php echo $row['product_title'] ?></h4>
                                            <p>Unit Price: <?php echo $row['unite_price'] ?></p>
                                        </div>
                                    </td>
                                    <form method="post" action="cart_add.php">
                                    <td class="quy-col">
                                        <div class="quantity">
                                            <div class="pro-qty">
                                                <input type="text" name="qty" value="<?php echo $row['qty'] ?>">
                                            </div>
                                        </div>
                                    </td>
                                    <td class="total-col"><h4><?php echo $row['total_price'];
                                            $_SESSION['total']=$_SESSION['total']+$row['total_price'];?></h4></td>
                                    <td class="total-col">
                                        <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                        <button name="update" class='btn btn-success btn-sm edit btn-flat' ><i class='fa fa-edit'></i> Update</button>
                                        <button name="delete" class='btn btn-danger btn-sm delete btn-flat'><i class='fa fa-trash'></i> Delete</button>
                                    </td>
                                    </form>
                                </tr>
                                <?php
                            }
                            ?>
							</tbody>
						</table>
						</div>
						<div class="total-cost">
							<h6>Total <span><?php  echo $_SESSION['total'] ?></span></h6>
						</div>
					</div>
				</div>
				<div class="col-lg-4 card-right">
                    <?php
                    if(!isset($_SESSION['UserID'])) echo "<p>You need to <b>Login</b> to checkout.</p>";
					else {
					    if($_SESSION['total'])
					    echo "<a href='checkout.php' class='site-btn'>Proceed to checkout</a>";
					    else echo "<p>Your cart is empty.</p>";
                    }
					?>
					<a href="" class="site-btn sb-dark">Continue shopping</a>
				</div>
			</div>
		</div>
	</section>
	<!-- cart section end -->

	<!-- Related product section -->
	<section class="related-product-section">
		<div class="container">
			<div class="section-title text-uppercase">
				<h2>Continue Shopping</h2>
			</div>
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<div class="tag-new">New</div>
							<img src="./img/product/2.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Black and White Stripes Dress</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="./img/product/5.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Flamboyant Pink Top </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="./img/product/9.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Flamboyant Pink Top </p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item">
						<div class="pi-pic">
							<img src="./img/product/1.jpg" alt="">
							<div class="pi-links">
								<a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$35,00</h6>
							<p>Flamboyant Pink Top </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Related product section end -->



    <!-- Footer section -->
    <?php include_once '../views/elements/footer.php' ?>
    <!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.nicescroll.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/main.js"></script>

	</body>
</html>
