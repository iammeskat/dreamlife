<?php include('connection.php');
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>DreamLife | eCommerce Website</title>
    <?php include_once '../views/elements/head.php' ?>
    <style>
        #ppic{
            width: 230px;
            height: 300px;
        }
    </style>



</head>
<?php
if(array_key_exists('guest',$_SESSION)&&!empty($_SESSION['guest']));
else $_SESSION['guest']=rand(111,999).time();
//echo $_SESSION['guest'];
?>

<?php
include_once 'connect.php';
include_once 'classes/product.php';
$product = new product($pdo);
?>

<body>
	<!-- Page Preloder -->
         <?php include_once '../views/elements/loader.php' ?>
    <!-- Page Preloder end -->

    <!-- Header section -->
    <?php include_once '../views/elements/nav.php' ?>
    <!-- End Header section -->



	<!-- Hero section -->
    <?php include_once '../views/elements/carousel.php' ?>
	<!-- Hero section end -->



	<!-- Features section -->
	<section class="features-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 p-0 feature">
					<div class="feature-inner">
						<div class="feature-icon">
							<img src="img/icons/1.png" alt="#">
						</div>
						<h2>Fast Secure Payments</h2>
					</div>
				</div>
				<div class="col-md-4 p-0 feature">
					<div class="feature-inner">
						<div class="feature-icon">
							<img src="img/icons/2.png" alt="#">
						</div>
						<h2>Popular Products</h2>
					</div>
				</div>
				<div class="col-md-4 p-0 feature">
					<div class="feature-inner">
						<div class="feature-icon">
							<img src="img/icons/3.png" alt="#">
						</div>
						<h2>Free & fast Delivery</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Features section end -->


	<!-- letest product section -->
	<section class="top-letest-product-section">
		<div class="container">
			<div class="section-title">
				<h2>LATEST PRODUCTS</h2>
			</div>
			<div class="product-slider owl-carousel">
                <?php
                $data = $product->LatestProduct();
                $i=1;
                foreach ($data as $row) {
                    ?>
                    <div class="product-item" style="width: 230px">
                        <div class="pi-pic">
                            <a href="product.php?id=<?php echo $row['id']; ?>"><img id="ppic" src="<?php echo $row['picture']; ?>" alt=""></a>
                            <div class="pi-links">
                                <a href="cart_add.php?add=<?php echo $row['id']; ?>" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                                <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                            </div>
                        </div>
                        <div class="pi-text">
                            <h6>$<?php echo $row['mrp']; ?></h6>
                            <p><?php echo $row['title']; ?></p>
                        </div>
                    </div>
                    <?php
                    if($i==7) break;
                    $i++;
                }
                ?>

			</div>
		</div>
	</section>
	<!-- letest product section end -->



	<!-- Product filter section -->
	<section class="product-filter-section">
		<div class="container">
			<div class="section-title">
				<h2>BROWSE TOP SELLING PRODUCTS</h2>
			</div>
			<ul class="product-filter-menu">
				<li><a href="#">TOPS</a></li>
				<li><a href="#">JUMPSUITS</a></li>
				<li><a href="#">LINGERIE</a></li>
				<li><a href="#">JEANS</a></li>
				<li><a href="#">DRESSES</a></li>
				<li><a href="#">COATS</a></li>
				<li><a href="#">JUMPERS</a></li>
				<li><a href="#">LEGGINGS</a></li>
			</ul>
			<div class="row">
                <?php
                $data = $product->TopSell();
                $i=1;
                foreach ($data as $row) {
                    ?>
				<div class="col-lg-3 col-sm-6">
					<div class="product-item" style="width: 230px">
						<div class="pi-pic">
                            <a href="product.php?id=<?php echo $row['id']; ?>"><img id="ppic" src="<?php echo $row['picture']; ?>" alt=""></a>
							<div class="pi-links">
								<a href="cart_add.php?add=<?php echo $row['id']; ?>" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
								<a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
							</div>
						</div>
						<div class="pi-text">
							<h6>$<?php echo $row['mrp']; ?></h6>
							<p><?php echo $row['title']; ?></p>
						</div>
					</div>
				</div>
                <?php
                if($i==7) break;
                $i++;
                }
                ?>
			</div>
			<div class="text-center pt-5">
				<button class="site-btn sb-line sb-dark">LOAD MORE</button>
			</div>
		</div>
	</section>
	<!-- Product filter section end -->


	<!-- Banner section -->
    <?php include_once '../views/elements/banner.php' ?>
	<!-- Banner section end  -->


	<!-- Footer section -->
    <?php include_once '../views/elements/footer.php' ?>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.slicknav.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.nicescroll.min.js"></script>
	<script src="js/jquery.zoom.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/main.js"></script>

	</body>
</html>
