-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 17, 2019 at 09:51 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dreamlife`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `picture`, `email`, `password`, `phone`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(100, 'Meskatul Islam', 'meskat', 'img/admin/2_sdBLtO_400x400.png', 'meskat@live.com', '1212', '01861932523', 1, 1, '0000-00-00 00:00:00', '2019-08-17 02:23:00'),
(101, 'Md Masud Rana', 'masud', '', 'masud@mail.com', '1212', '', 0, 0, '2019-08-17 12:18:32', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `max_display` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `picture`, `link`, `promotional_message`, `html_banner`, `is_active`, `is_draft`, `soft_delete`, `max_display`, `created_at`, `modified_at`) VALUES
(101, 'Samsung Electronics', 'img/banner/bg.jpg', 'product.php?id=112', 'a', 'snbdfgs sbdfhgj nsbdf', 1, 1, 0, 1, '2019-07-30 10:28:25', '2019-08-17 07:52:57'),
(102, 'Walton Hi Tech Ind.', 'img/banner/bg-2.jpg', 'www.samsung.com', 'sjbjfgh nmbhjdfg jsbdhufg', 'snbdfgs sbdfhgj nsbdf', 0, 1, 1, 1, '2019-07-30 10:29:06', '2019-07-31 04:02:51'),
(104, 'Samsung Notebook 9', 'img/banner/164040.jpg', 'product.php?id=118', 'sry srty ', 'srty srty ', 1, 1, 0, 1, '2019-07-31 12:50:33', '2019-08-17 07:53:15'),
(105, 'Apple Inc.', 'img/banner/165281.jpg', 'product.php?id=117', 'zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg v', 'jbdfgjdxhjg', 1, 1, 0, 1, '2019-07-31 12:52:01', '2019-08-17 07:52:36'),
(106, 'Walton Hi Tech Ind.', 'img/banner/181141.jpg', 'product.php?id=116', 'zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg zjhejdfjkg jhzbdjkfg jbzjkdg v', 'bzdhfjgz hjbgjd jnsjugn', 1, 1, 0, 1, '2019-07-31 01:05:54', '2019-08-17 07:52:20'),
(107, 'ghs jhahejrtkog aaaa', 'img/banner/1-161126224004.jpg', 'w5y', 'wr5y', 'wryh', 0, 1, 0, 1, '2019-08-05 12:16:24', '2019-08-17 02:08:48'),
(108, 'hghrg', 'img/banner/221775.jpg', 'product.php?id=115', 'dfhgj', 'dhfkj', 1, 1, 0, 1, '2019-08-17 02:25:43', '2019-08-17 07:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `is_draft` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `soft_delete` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `title`, `link`, `is_draft`, `is_active`, `soft_delete`, `created_at`, `modified_at`) VALUES
(100, 'Samsung Electronics', 'www.samsung.com', 0, 0, 0, '2019-07-29 09:53:37', '2019-08-03 09:31:59'),
(101, 'Apple Inc.', 'www.apple.com', 1, 1, 0, '2019-07-29 10:09:23', '2019-07-29 10:09:23'),
(102, 'Microsoft Corporation', 'www.microsoft.com', 1, 1, 1, '2019-07-29 10:10:06', '2019-07-29 10:10:06'),
(103, 'Walton Hi Tech Ind.', 'www.walton.com.bd', 1, 1, 1, '2019-07-29 10:11:27', '2019-07-29 10:11:27'),
(106, 'Adidas 2', 'jansdjf', 1, 1, 1, '2019-07-31 05:37:46', '2019-08-05 12:54:45'),
(107, 'Supreme', 'jhbsghdf', 1, 1, 1, '2019-07-31 05:37:57', NULL),
(108, 'Nike', 'jsnadf', 1, 1, 1, '2019-07-31 05:38:09', NULL),
(110, 'drthrddth', 'dthgd', 1, 1, 1, '2019-08-05 12:54:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `sid` varchar(20) NOT NULL,
  `product_id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `unite_price` int(11) NOT NULL,
  `total_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `sid`, `product_id`, `picture`, `product_title`, `qty`, `unite_price`, `total_price`) VALUES
(26, '3841565956958', 106, 'img/product/2.jpg', 'Flamboyant gray Top ', 1, 30000, 30000),
(27, '3841565956958', 107, 'img/product/3.jpg', 'Flamboyant dark blue Top', 4, 5000, 20000),
(28, '3841565956958', 108, 'img/product/4.jpg', 'Flamboyant yellow Top', 2, 10000, 20000),
(29, '3841565956958', 109, 'img/product/5.jpg', 'Flamboyant silver saree Top', 5, 1111, 5555),
(30, '3841565956958', 112, 'img/product/8.jpg', 'Flamboyant orange Top', 1, 10000, 10000),
(31, '3841565956958', 111, 'img/product/7.jpg', 'Flamboyant black saree', 1, 10000, 10000),
(32, '3841565956958', 110, 'img/product/6.jpg', 'Flamboyant black & red saree Top', 3, 10000, 30000),
(33, '3841565956958', 118, 'img/product/103a1c452875586b5fc8c66023ea17c9.0.jpg', 'Samsung Electronics', 1, 99999, 99999),
(34, '3841565956958', 117, 'img/product/164461-1.jpg', 'Canon DSLR Camera', 1, 80000, 80000),
(35, '8491566049484', 109, 'img/product/5.jpg', 'Flamboyant silver saree Top', 1, 1111, 1111),
(36, '8491566049484', 117, 'img/product/164461-1.jpg', 'Canon DSLR Camera', 3, 80000, 240000),
(37, '8491566049484', 118, 'img/product/103a1c452875586b5fc8c66023ea17c9.0.jpg', 'Samsung Electronics', 1, 99999, 99999),
(38, '8491566049484', 106, 'img/product/2.jpg', 'Flamboyant gray Top ', 1, 30000, 30000),
(39, '8491566049484', 107, 'img/product/3.jpg', 'Flamboyant dark blue Top', 5, 5000, 25000),
(40, '8491566049484', 108, 'img/product/4.jpg', 'Flamboyant yellow Top', 1, 10000, 10000),
(41, '8491566049484', 110, 'img/product/6.jpg', 'Flamboyant black & red saree Top', 1, 10000, 10000),
(42, '8491566049484', 112, 'img/product/8.jpg', 'Flamboyant orange Top', 2, 10000, 20000),
(43, '8491566049484', 114, 'img/product/10.jpg', 'Flamboyant orange saree', 1, 99999, 99999),
(44, '8581566050682', 108, 'img/product/4.jpg', 'Flamboyant yellow Top', 1, 10000, 10000),
(45, '8581566050682', 113, 'img/product/9.jpg', 'Flamboyant red sareeeee', 1, 99999, 99999),
(46, '8581566050682', 117, 'img/product/164461-1.jpg', 'Canon DSLR Camera', 1, 80000, 80000),
(47, '5631566054418', 107, 'img/product/3.jpg', 'Flamboyant dark blue Top', 1, 5000, 5000),
(48, '9361566055339', 112, 'img/product/8.jpg', 'Flamboyant orange Top', 1, 10000, 10000),
(49, '9361566055339', 117, 'img/product/164461-1.jpg', 'Canon DSLR Camera', 3, 80000, 240000),
(50, '9361566055339', 115, 'img/product/11.jpg', 'Flamboyant blue Top', 1, 10000, 10000),
(51, '9361566055339', 110, 'img/product/6.jpg', 'Flamboyant black & red saree Top', 1, 10000, 10000),
(52, '9361566055339', 111, 'img/product/7.jpg', 'Flamboyant black saree', 1, 10000, 10000),
(53, '9361566055339', 114, 'img/product/10.jpg', 'Flamboyant orange saree', 1, 99999, 99999),
(54, '9361566055339', 118, 'img/product/103a1c452875586b5fc8c66023ea17c9.0.jpg', 'Samsung Electronics', 1, 99999, 99999),
(55, '2611566056092', 118, 'img/product/103a1c452875586b5fc8c66023ea17c9.0.jpg', 'Samsung Electronics', 1, 99999, 99999),
(56, '2611566056092', 117, 'img/product/164461-1.jpg', 'Canon DSLR Camera', 1, 80000, 80000),
(57, '2611566056092', 116, 'img/product/12.jpg', 'Flamboyant hg jdfi Top', 1, 99999, 99999),
(58, '2611566056092', 114, 'img/product/10.jpg', 'Flamboyant orange saree', 1, 99999, 99999),
(59, '2611566056092', 109, 'img/product/5.jpg', 'Flamboyant silver saree Top', 4, 1111, 4444),
(60, '2611566056092', 113, 'img/product/9.jpg', 'Flamboyant red sareeeee', 1, 99999, 99999),
(61, '2611566056092', 112, 'img/product/8.jpg', 'Flamboyant orange Top', 1, 10000, 10000),
(62, '2611566056092', 111, 'img/product/7.jpg', 'Flamboyant black saree', 1, 10000, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `c_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `soft_delete` tinyint(1) DEFAULT NULL,
  `is_draft` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `c_name`, `link`, `soft_delete`, `is_draft`, `created_at`, `modified_at`) VALUES
(3, 'Men', 'njhseffjhg', 1, 1, '2019-07-31 02:04:11', '2019-07-31 03:53:10'),
(4, 'Women', 'njhseffjhg', 1, 1, '2019-07-31 01:09:07', '2019-07-31 03:57:54'),
(5, 'Shoes', 'njhseffjhg', 1, 1, '2019-07-31 01:09:16', '2019-07-31 03:58:10'),
(6, 'Electronics', 'njhseffjhg', 1, 1, '2019-07-31 01:09:34', '2019-07-31 03:58:22'),
(7, 'Furniture', 'njhseffjhg', 1, 1, '2019-07-31 01:09:44', '2019-07-31 03:58:35'),
(8, 'Food and Beverage', 'njhseffjhg', 1, 1, '2019-07-31 01:09:59', '2019-07-31 03:58:45'),
(9, 'Pharmaceuticals', 'njhseffjhg', 1, 1, '2019-07-31 01:10:12', '2019-07-31 03:59:03'),
(10, 'Science & Technology', 'njhseffjhg', 1, 1, '2019-07-31 01:10:31', '2019-07-31 03:59:14'),
(11, 'book', 'njhseffjhg', 1, 1, '2019-07-31 01:12:21', '2019-08-01 02:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `bid` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `country` varchar(50) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `total_price` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checkout`
--

INSERT INTO `checkout` (`id`, `sid`, `bid`, `address`, `address2`, `country`, `zip`, `phone`, `total_price`, `date`, `status`) VALUES
(8, '3841565956958', 100, 'Kazi Bari, Shovandandi, Patiya', 'Chittagong', 'Bangladesh', '4370', '01861932523', 305554, '2019-08-16 11:39:33', 1),
(9, '8491566049484', 103, 'Shovandandin,Patiya, Chittagong', 'Chittagong', 'Bangladesh', '4370', '01770724109', 536109, '2019-08-17 08:04:41', 1),
(12, '9361566055339', 103, 'Shovandandin,Patiya, Chittagong', 'Chittagong', 'Bangladesh', '4370', '01770724109', 479998, '2019-08-17 09:34:52', 0),
(13, '2611566056092', 100, 'Shovandandin,Patiya, Chittagong', 'Chittagong', 'Bangladesh', '4370', '01770724109', 504440, '2019-08-17 09:39:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `soft_delete` int(1) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `comment`, `status`, `soft_delete`, `date_time`) VALUES
(102, 'Meskatul Islam', 'meskat@live.com', 'for test', 'Hello Dreamlife...', 0, 1, '2019-07-31 11:34:42'),
(103, 'Mohammad Ramim', 'ramin@gmail.com', 'Test', 'Hello', 1, 1, '2019-07-31 11:35:02'),
(104, 'Sakib Al Hasan', 'sakib@mail.com', 'Test', 'kjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\nkjherjt jert  uhrt iuhty \r\n', 0, 1, '2019-08-01 10:55:34');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `title`, `picture`) VALUES
(4, 'Label 1', 'img/labels/6e3d61bd34d792474470f0e8a442d388.0.jpg'),
(100, 'Label 2', 'img/labels/103a1c452875586b5fc8c66023ea17c9.0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `map_product_tag`
--

CREATE TABLE `map_product_tag` (
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `popular_tag`
--

CREATE TABLE `popular_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `brand_id` int(11) NOT NULL,
  `lebel_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `total_sales` int(11) NOT NULL,
  `product_type` varchar(255) NOT NULL,
  `is_new` tinyint(4) NOT NULL,
  `cost` float NOT NULL,
  `mrp` float NOT NULL,
  `special_price` float NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `lebel_id`, `title`, `picture`, `short_description`, `description`, `total_sales`, `product_type`, `is_new`, `cost`, `mrp`, `special_price`, `soft_delete`, `is_draft`, `is_active`, `created_at`, `modified_at`) VALUES
(105, 105, 103, ' blue & black Top ', 'img/product/1.jpg', 'jhdt jhdg jahdg kahdjg akjndggvvvvvvv', 'ahdg abdg akdnfg akjdjnfgk akdnfgka djfgnka gds gdsjahdg abdg akdnfg akjdjnfgk akdnfgka djfgnka gds gdsjahdg abdg akdnfg akjdjnfgk akdnfgka djfgnka gds gdsjahdg abdg akdnfg akjdjnfgk akdnfgka djfgnka gds gdsjahdg abdg akdnfg akjdjnfgk akdnfgka djfgnka gds gdsj', 0, 'Women', 1, 40000, 35000, 33000, 0, 0, 0, '2019-07-31 05:46:13', '2019-08-18 01:41:28'),
(106, 107, 103, 'Flamboyant gray Top ', 'img/product/2.jpg', 'zkjdgkis mdnfmg', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 25400, 30000, 28000, 0, 1, 1, '2019-07-31 05:48:03', '0000-00-00 00:00:00'),
(107, 108, 103, 'Flamboyant dark blue Top', 'img/product/3.jpg', 'zkjdgkis mdnfmg', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 4545, 5000, 48000, 0, 1, 1, '2019-07-31 05:49:10', '0000-00-00 00:00:00'),
(108, 106, 103, 'Flamboyant yellow Top', 'img/product/4.jpg', 'zkjdgkis mdnfmg', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 4545, 10000, 14500, 0, 1, 1, '2019-07-31 05:50:02', '0000-00-00 00:00:00'),
(109, 107, 103, 'Flamboyant silver saree Top', 'img/product/5.jpg', 'zkjdgkis mdnfmg', 'uiyim iuhajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 9999, 1111, 999, 0, 1, 1, '2019-07-31 05:51:08', '0000-00-00 00:00:00'),
(110, 106, 103, 'Flamboyant black & red saree Top', 'img/product/6.jpg', 'jrk ksjertg jsen', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 4545, 10000, 111111, 0, 1, 1, '2019-07-31 05:52:11', '0000-00-00 00:00:00'),
(111, 106, 103, 'Flamboyant black saree', 'img/product/7.jpg', 'znjfndz', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 999999, 10000, 111111, 0, 1, 1, '2019-07-31 05:55:16', '0000-00-00 00:00:00'),
(112, 107, 103, 'Flamboyant orange Top', 'img/product/8.jpg', 'asfg f', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 999999, 10000, 14500, 0, 1, 1, '2019-07-31 05:57:31', '0000-00-00 00:00:00'),
(113, 108, 103, 'Flamboyant red sareeeee', 'img/product/9.jpg', 'znjfndz', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 4545, 99999, 9999, 0, 1, 1, '2019-07-31 05:58:42', '0000-00-00 00:00:00'),
(114, 107, 103, 'Flamboyant orange saree', 'img/product/10.jpg', 'asghj m ks kkrd', 'njeg kjdfh ksjdkh', 0, 'Women', 1, 9999, 99999, 111111, 0, 1, 1, '2019-07-31 05:59:47', '0000-00-00 00:00:00'),
(115, 108, 103, 'Flamboyant blue Top', 'img/product/11.jpg', 'dyje paoitm kkr', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 1, 9999, 10000, 14500, 0, 1, 1, '2019-07-31 06:00:42', '0000-00-00 00:00:00'),
(116, 107, 103, 'Flamboyant hg jdfi Top', 'img/product/12.jpg', 'iu45 gyuyut jahuie', 'ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ajifgjnoijifghnioj ioj iijty kkd dijy klj ', 0, 'Women', 0, 9999, 99999, 9999, 0, 1, 1, '2019-07-31 06:01:46', '0000-00-00 00:00:00'),
(117, 109, 102, 'Canon DSLR Camera', 'img/product/164461-1.jpg', 'Canon DSLR Camera jhdjk kjhekrg hdiug', 'Canon DSLR Camera jhdjk kjhekrg hdiug Canon DSLR Camera jhdjk kjhekrg hdiug Canon DSLR Camera jhdjk kjhekrg hdiug', 0, 'Electronics', 1, 75000, 80000, 78000, 0, 1, 1, '2019-07-31 08:05:56', '0000-00-00 00:00:00'),
(118, 101, 1, 'Samsung Electronics', 'img/product/103a1c452875586b5fc8c66023ea17c9.0.jpg', 'Samsung laptop', 'java the complete references', 0, 'Men', 1, 9999, 99999, 14500, 1, 1, 1, '2019-08-05 01:39:19', '2019-08-05 01:39:19');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `promotional_message` varchar(255) NOT NULL,
  `html_banner` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcribers`
--

CREATE TABLE `subcribers` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_subcribed` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_draft` tinyint(1) NOT NULL,
  `soft_delete` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` varchar(8) NOT NULL,
  `dob` date NOT NULL,
  `religion` varchar(20) NOT NULL,
  `city` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `modified_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `password` varchar(50) NOT NULL,
  `picture` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `username`, `email`, `phone`, `gender`, `dob`, `religion`, `city`, `address`, `is_active`, `modified_at`, `created_at`, `password`, `picture`) VALUES
(100, 'Meskatul Islam', 'meskat', 'meskat@live.com', '01861932523', 'Male', '1997-09-17', 'Islam', 'Chittagong', 'Shovandandi,Patiya, Chittagong', 1, '2019-08-17 06:43:11', '2019-08-06 12:42:03', '1212', 'img/users/2_sdBLtO_400x400.png'),
(101, 'Md. Ramim', 'ramim', 'ramim@mail.com', '01754658781', 'Male', '2019-08-15', 'Islam', 'Chittagong', 'Chandgaon, Chittagong', 1, '0000-00-00 00:00:00', '2019-08-15 12:05:04', '1212', 'img/users/desktop-3d-nature-wallpaper-240x320-dowload copy.jpg'),
(102, 'Md. Ramim', 'ramim2', 'ramim@mail.com1', '01754658781', 'Male', '2019-08-15', 'Islam', 'Chittagong', 'Chandgaon, Chittagong', 1, '0000-00-00 00:00:00', '2019-08-15 12:11:46', '1212', 'img/users/MESKATUL_ISLAM_Phone_ll.jpg'),
(103, 'Md. Masud', 'masud', 'masud@mail.com', '01770724109', 'Male', '1997-08-17', 'Islam', 'Chittagong', 'Shovandandin,Patiya, Chittagong', 1, '0000-00-00 00:00:00', '2019-08-17 08:02:11', '1212', 'img/users/6e3d61bd34d792474470f0e8a442d388.0.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_product_tag`
--
ALTER TABLE `map_product_tag`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `popular_tag`
--
ALTER TABLE `popular_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcribers`
--
ALTER TABLE `subcribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `map_product_tag`
--
ALTER TABLE `map_product_tag`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `popular_tag`
--
ALTER TABLE `popular_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subcribers`
--
ALTER TABLE `subcribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
